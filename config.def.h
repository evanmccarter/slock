/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nobody";

static const char *colorname[NUMCOLS] = {
	[INIT]        = "#000000", /* after initialization */
	[INPUT]       = "#00FFFF", /* during input */
	[FAILED]      = "#330033", /* wrong password */
	[FAILED2]     = "#660066", /* 2 wrong passwords */
	[FAILED3]     = "#990099", /* 3 wrong passwords */
	[FAILED4]     = "#CC00CC", /* 4 wrong passwords */
	[FAILEDMAX]   = "#FF00FF", /* max wrong passwords */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
